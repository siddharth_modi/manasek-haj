//
//  AppDelegate.h
//  Resaleh
//
//  Created by siddharth on 10/12/16.
//  Copyright © 2016 qtm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (retain, nonatomic) UINavigationController *nav;
+(AppDelegate *)sharedAppDelegate;
-(void)showFavouriteView;
-(void)showHomeView;
-(void)showAboutView;
-(void)showAboutSoftwareView;
-(void)showGuideView;
-(void)showsearchView;
-(void)copydatabaseifneeded;
-(NSString *)getdatabase;
@end

