//
//  RearViewController.h
//  Resaleh
//
//  Created by siddharth on 10/12/16.
//  Copyright © 2016 qtm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RearViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tbl_sidemenu;
@end
