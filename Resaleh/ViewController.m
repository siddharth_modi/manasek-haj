//
//  ViewController.m
//  Resaleh
//
//  Created by siddharth on 10/12/16.
//  Copyright © 2016 qtm. All rights reserved.
//

#import "ViewController.h"
#import "DetailViewController.h"
#import "MVYSideMenuController.h"
#import "AllTableViewCell.h"
#import "AppDelegate.h"
#import <sqlite3.h>
#import "UIImage+animatedGIF.h"
#import "GuideViewController.h"
#import "FavouriteViewController.h"
#import "AboutUsViewController.h"
#import "AboutSoftwareViewController.h"
#import "SearchViewController.h"


@interface ViewController ()
{
    sqlite3 *database;
    NSMutableArray *allname;
    NSMutableArray *allID;
    NSMutableArray *arr_Comment;
    NSMutableArray *arr_ChildCount;
    NSInteger openFlag;
    NSString *str_uid;
    NSMutableDictionary *data;
    
    NSMutableArray *searchArray;
    NSString *searchTextString;
    NSMutableDictionary *search_Result;
    NSMutableArray *searchResult;
    
    BOOL isSearch;
}
@property (weak, nonatomic) IBOutlet UITextField *txt_search;

@property NSMutableArray *objects;
@property (nonatomic, retain) NSMutableArray *displayArray;
@end
NSString *const keyIndent = @"indent";
NSString *const keyTitle = @"title";
NSString *const keyChildren = @"keyChildren";
NSString *const keyID = @"ID";
NSString *const KeyChildCount = @"KeyChildCount";
NSString *const KeyComment = @"KeyComment";
NSString *const KeyParentID = @"KeyParentID";
NSString *const KeyFileOrFolder = @"KeyFileOrFolder";





@implementation ViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];

    self.navigationController.navigationBarHidden=YES;
    NSString *str_launch = [[NSUserDefaults standardUserDefaults] objectForKey:@"LaunchApp"];
    
    NSMutableArray *all_index=[[NSMutableArray alloc] init];
    all_index=[[NSUserDefaults standardUserDefaults] objectForKey:@"AllIndex"];
    
    if (str_launch==nil)
    {
        _view_video.hidden=NO;
        
        [[NSUserDefaults standardUserDefaults] setValue:@"Yes" forKey:@"LaunchApp"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        NSURL *url = [[NSBundle mainBundle] URLForResource:@"splash1" withExtension:@"gif"];
        UIImage *testImage = [UIImage animatedImageWithAnimatedGIFData:[NSData dataWithContentsOfURL:url]];
        self.img_video.animationImages = testImage.images;
        self.img_video.animationDuration = testImage.duration;
        self.img_video.animationRepeatCount = 1;
        self.img_video.image = testImage.images.lastObject;
        [self.img_video startAnimating];
        
        [self performSelector:@selector(subscribe) withObject:self afterDelay:15.0];

        NSLog(@"Hello");
    }
    else
    {
        _view_video.hidden=YES;

    }
    
     self.view.backgroundColor = [UIColor colorWithPatternImage: [UIImage imageNamed:@"tiledBG"]];
    [_btn_sidemenu addTarget:self action:@selector(OpenSideMenu) forControlEvents:UIControlEventTouchUpInside];

    if (all_index.count==0 || all_index==nil)
    {
        data=[self FetchData:[NSString stringWithFormat:@"SELECT * FROM WebSite_Category where ParentID=0"]];
        
        [self.txt_search addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
        
        isSearch=NO;
        search_Result=[[NSMutableDictionary alloc] init];
        _objects=[[NSMutableArray alloc] init];
        NSArray *names=[data valueForKey:@"Name"];
        NSArray *ids=[data valueForKey:@"Ids"];
        NSArray *childcount=[data valueForKey:@"Childcount"];
        NSArray *Comment=[data valueForKey:@"Comment"];
        NSArray *ParentID=[data valueForKey:@"ParentId"];
        NSArray *fileOrFolder=[data valueForKey:@"FileOrFolder"];
        
        
        for (int i=0; i<[names count]; i++)
        {
            NSDictionary *arraydata=
            @{
              keyIndent:@0,
              keyTitle:[names objectAtIndex:i],
              keyID:[ids objectAtIndex:i],
              KeyChildCount:[childcount objectAtIndex:i],
              KeyComment:[Comment objectAtIndex:i],
              KeyParentID:[ParentID objectAtIndex:i],
              KeyFileOrFolder:[fileOrFolder objectAtIndex:i],
              keyChildren:@[]
              };
            
            [_objects addObject:arraydata];
        }
        
        NSLog(@" all data %@",_objects);
        [self updateSearchArray];
    }
    else
    {
        _objects=all_index.mutableCopy;
       // [_tbl_alldata reloadData];
    }
    [_tbl_alldata reloadData];
   
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(pushguideVC) name:@"pushguideVC" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(pushsearchVC) name:@"pushsearchVC" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(pushfavouriteVC) name:@"pushfavouriteVC" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(pushAboutUSVC) name:@"pushAboutUSVC" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(pushAboutSoftwareVC) name:@"pushAboutSoftwareVC" object:nil];
   
    // Do any additional setup after loading the view, typically from a nib.
}
-(void)pushguideVC
{
    GuideViewController *vc=[self.storyboard instantiateViewControllerWithIdentifier:@"GuideViewController"];
    [self.navigationController pushViewController:vc animated:YES];
}
-(void)pushsearchVC
{
    SearchViewController *vc=[self.storyboard instantiateViewControllerWithIdentifier:@"SearchViewController"];
    [self.navigationController pushViewController:vc animated:YES];
}
-(void)pushfavouriteVC
{
    FavouriteViewController *vc=[self.storyboard instantiateViewControllerWithIdentifier:@"FavouriteViewController"];
    [self.navigationController pushViewController:vc animated:YES];
}
-(void)pushAboutUSVC
{
    AboutUsViewController *vc=[self.storyboard instantiateViewControllerWithIdentifier:@"AboutUsViewController"];
    [self.navigationController pushViewController:vc animated:YES];
}
-(void)pushAboutSoftwareVC
{
    AboutSoftwareViewController *vc=[self.storyboard instantiateViewControllerWithIdentifier:@"AboutSoftwareViewController"];
    [self.navigationController pushViewController:vc animated:YES];
}
-(void)subscribe
{
    NSLog(@"Hello");
    _view_video.hidden=YES;

}
- (void)viewDidUnload
{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
    [super viewDidUnload];
}
-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden=YES;
}

#pragma mark – Fetch Data in Database

-(NSMutableDictionary *)FetchData:(NSString *)querySQL
{
    
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
  
    AppDelegate *appDelegateLocal = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    const char *dbpath = [[appDelegateLocal getdatabase] UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSLog(@"%@",querySQL);
        const char *sqlStatement = [querySQL UTF8String];
        sqlite3_stmt *statement;
        if( sqlite3_prepare_v2(database, sqlStatement, -1, &statement, NULL) == SQLITE_OK )
        {
                NSMutableArray *allID1=[[NSMutableArray alloc] init];
                NSMutableArray *allname2=[[NSMutableArray alloc] init];
                NSMutableArray *arrComment=[[NSMutableArray alloc] init];
                NSMutableArray *ArrChildCount=[[NSMutableArray alloc] init];
                NSMutableArray *ArrParentID=[[NSMutableArray alloc] init];
                NSMutableArray *ArrFileORFolder=[[NSMutableArray alloc] init];



                while(sqlite3_step(statement) == SQLITE_ROW)
                {
                    char *field1 = (char *) sqlite3_column_text(statement,0);
                    NSString *field1Str = [[NSString alloc] initWithUTF8String: field1];
                    char *field2 = (char *) sqlite3_column_text(statement,1);
                    NSString *field2Str = [[NSString    alloc] initWithUTF8String: field2];
                    NSString *newString = [field2Str stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                    
                    char *field3 = (char *) sqlite3_column_text(statement,2);
                    NSString *FieldComment;
                    if (field3!=nil)
                    {
                        FieldComment = [[NSString alloc] initWithUTF8String: field3];
                    }
                    else
                    {
                        FieldComment=@"";
                    }
                   

                    char *fieldParentID = (char *) sqlite3_column_text(statement,14);
                    NSString *str_ParentID = [[NSString alloc] initWithUTF8String: fieldParentID];
                    char *field4 = (char *) sqlite3_column_text(statement,16);
                    NSString *FieldChildCount = [[NSString alloc] initWithUTF8String: field4];
                    
                    NSString *Query=[NSString stringWithFormat:@"SELECT * FROM WebSite_Category where ParentID=%@",field1Str];
                        NSString *dicchilddata=[self FetchDataIsFileORFolder:Query];
                    NSLog(@"%@",dicchilddata);
                    NSString *Component;
                    if (dicchilddata==nil)
                    {
                        Component=@"File";

                    }
                    else
                    {
                        Component=@"Folder";

                    }
                    
                    //
                    //
                    //            NSArray *arr_id1=[dicchilddata valueForKey:@"ParentId"];
                    
                    [allID1 addObject:field1Str];
                    [allname2 addObject:newString];
                    [arrComment addObject:FieldComment];
                    [ArrChildCount addObject:FieldChildCount];
                    [ArrParentID addObject:str_ParentID];
                    [ArrFileORFolder addObject:Component];
                    
                }
                [dic setValue:allname2 forKey:@"Name"];
                [dic setValue:allID1 forKey:@"Ids"];
                [dic setValue:arrComment forKey:@"Comment"];
                [dic setValue:ArrChildCount forKey:@"Childcount"];
                [dic setValue:ArrParentID forKey:@"ParentId"];
                [dic setValue:ArrFileORFolder forKey:@"FileOrFolder"];

                NSLog(@"dictioary is %@",dic);
        }
        else
        {

            NSLog( @"Failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(database));
        }
        sqlite3_finalize(statement);
        sqlite3_close(database);
        
    }
    return dic;
}
-(NSString *)FetchDataIsFileORFolder:(NSString *)querySQL
{
    NSString *ParentID;
    AppDelegate *appDelegateLocal = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    const char *dbpath = [[appDelegateLocal getdatabase] UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSLog(@"%@",querySQL);
        const char *sqlStatement = [querySQL UTF8String];
        sqlite3_stmt *statement;
        if( sqlite3_prepare_v2(database, sqlStatement, -1, &statement, NULL) == SQLITE_OK )
        {
           
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                char *field1 = (char *) sqlite3_column_text(statement,14);
                if (field1!=nil)
                {
                    ParentID = [[NSString alloc] initWithUTF8String: field1];
                    
                }
                else
                {
                    ParentID=@"";
                }

            }

            
            
            NSLog(@"dictioary is %@",ParentID);
        }
        else
        {
            
            NSLog( @"Failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(database));
        }
        sqlite3_finalize(statement);
        sqlite3_close(database);
        
    }
    return ParentID;
}

#pragma mark – Sidemenu method
-(void)OpenSideMenu
{
    MVYSideMenuController *sideMenuController = [self sideMenuController];
    if (sideMenuController)
    {
        [sideMenuController openMenu];
    }
}
#pragma mark - TextField delegate


-(void)textFieldDidChange:(UITextField*)textField
{
    searchTextString = textField.text;
    NSString *str_Query=[NSString stringWithFormat:@"SELECT * FROM  WebSite_Category where CountChild=0 and Title like '%%%@%%'",searchTextString];
    search_Result=[self FetchData:str_Query];
    searchArray=[[NSMutableArray alloc] init];
    NSArray *names=[search_Result valueForKey:@"Name"];
    NSArray *ids=[search_Result valueForKey:@"Ids"];
    NSArray *childcount=[search_Result valueForKey:@"Childcount"];
    NSArray *Comment=[search_Result valueForKey:@"Comment"];
    for (int i=0; i<[names count]; i++)
    {
        NSDictionary *arraydata=
        @{
          keyIndent:@0,
          keyTitle:[names objectAtIndex:i],
          keyID:[ids objectAtIndex:i],
          KeyChildCount:[childcount objectAtIndex:i],
          KeyComment:[Comment objectAtIndex:i],
          keyChildren:@[]
          };
        
        [searchArray addObject:arraydata];
    }
    [self updateSearchArray];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    [textField resignFirstResponder];
    return YES;
}// called when 'return' key pressed. return NO to ignore.

//update seach method where the textfield acts as seach bar

#pragma mark – Choose Array For Tableview
-(void)updateSearchArray
{
    if (searchTextString.length != 0)
    {
        isSearch=YES;
    }
    else
    {
        isSearch=NO;
        [_txt_search resignFirstResponder];
    }
    [self.tbl_alldata reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark – Tableview Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (isSearch==YES)
    {
        return searchArray.count;
    }
    else
    {
        return _objects.count;

    }
    return 0;
}
// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
   AllTableViewCell  *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];

    if (isSearch==YES)
    {
        NSLog(@"%ld",(long)indexPath.row);
        NSString *newString = [[[searchArray objectAtIndex:indexPath.row] valueForKey:keyTitle]  stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        NSNumberFormatter *formatter = [NSNumberFormatter new];
        formatter.locale = [NSLocale localeWithLocaleIdentifier:@"ar"];
        for (NSInteger i = 0; i < 10; i++)
        {
            NSNumber *num = @(i);
            newString = [newString stringByReplacingOccurrencesOfString:num.stringValue withString:[formatter stringFromNumber:num]];
        }
        
        NSLog(@"%@",newString);
        cell.lbl_name.text =newString;
        
        CGRect frm=cell.img_close.frame;
        NSLog(@"%f",cell.contentView.frame.size.width);
        frm.origin.x=cell.contentView.frame.size.width-30;
        cell.img_close.frame=frm;
        
        CGRect frm_btn=cell.btn_Book.frame;
        frm_btn.origin.x=cell.contentView.frame.size.width-65;
        cell.btn_Book.frame=frm_btn;
        
        CGRect frm_lbl=cell.lbl_name.frame;
        frm_lbl.size.width=cell.contentView.frame.size.width-110;
        cell.lbl_name.frame=frm_lbl;
        CGRect frmbtn=cell.btn_Book.frame;
        frmbtn.size.width=25;
        frmbtn.size.height=30;
        cell.btn_Book.frame=frmbtn;
        [cell.btn_Book setImage:[UIImage imageNamed:@"File"] forState:UIControlStateNormal];
        
        CGRect frm_lbl1=cell.lbl_name.frame;
        frm_lbl1.size.width=cell.contentView.frame.size.width-80;
        cell.lbl_name.frame=frm_lbl1;
        [cell.lbl_name setFont:[UIFont fontWithName:@"system font" size:13]];
    }
    else
    {
        NSLog(@"%ld",(long)indexPath.row);
        NSString *newString = [[[_objects objectAtIndex:indexPath.row] valueForKey:keyTitle]  stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        NSNumberFormatter *formatter = [NSNumberFormatter new];
        formatter.locale = [NSLocale localeWithLocaleIdentifier:@"ar"];
        for (NSInteger i = 0; i < 10; i++)
        {
            NSNumber *num = @(i);
            newString = [newString stringByReplacingOccurrencesOfString:num.stringValue withString:[formatter stringFromNumber:num]];
        }
        NSLog(@"%@",newString);
        cell.lbl_name.text =newString;
        
        cell.indentationWidth = 20;
        NSInteger indentLevel = [[[_objects objectAtIndex:indexPath.row] valueForKey:keyIndent] integerValue];
        
        cell.indentationLevel =indentLevel;
        
        NSLog(@"%@",[_objects objectAtIndex:indexPath.row]);
        NSArray *allchild=[[_objects objectAtIndex:indexPath.row] valueForKey:keyChildren];
        
        CGRect frm=cell.btn_Book.frame;
        frm.size.width=30;
        frm.size.height=20;
        cell.btn_Book.frame=frm;
        
        [cell.img_close  setImage:[UIImage imageNamed:@"Close"]];
        [cell.btn_Book setImage:[UIImage imageNamed:@"CloseBook"] forState:UIControlStateNormal];
        if (allchild.count!=0)
        {
            [cell.btn_Book setImage:[UIImage imageNamed:@"OpenBook"] forState:UIControlStateNormal];
            [cell.img_close  setImage:[UIImage imageNamed:@"Open"]];
        }
        else
        {
            NSLog(@"%@",[[_objects objectAtIndex:indexPath.row] valueForKey:KeyFileOrFolder]);
            
            NSString *str_fileFolder=[[_objects objectAtIndex:indexPath.row] valueForKey:KeyFileOrFolder];
            
            if ([str_fileFolder  isEqualToString:@"File"])
            {
                CGRect frm=cell.btn_Book.frame;
                frm.size.width=25;
                frm.size.height=30;
                cell.btn_Book.frame=frm;
                [cell.btn_Book setImage:[UIImage imageNamed:@"File"] forState:UIControlStateNormal];
            }
            else
            {
                [cell.img_close  setImage:[UIImage imageNamed:@"Close"]];
                CGRect frm=cell.btn_Book.frame;
                frm.size.width=30;
                frm.size.height=20;
                cell.btn_Book.frame=frm;
            }
            
            
        }
//            int flag=[[[_objects objectAtIndex:indexPath.row] valueForKey:KeyChildCount] intValue];
//            if (flag==0)
//            {
//                CGRect frm=cell.btn_Book.frame;
//                frm.size.width=25;
//                frm.size.height=30;
//                cell.btn_Book.frame=frm;
//                [cell.btn_Book setImage:[UIImage imageNamed:@"File"] forState:UIControlStateNormal];
//            }
//            else
//            {
//                [cell.img_close  setImage:[UIImage imageNamed:@"Close"]];
//                CGRect frm=cell.btn_Book.frame;
//                frm.size.width=30;
//                frm.size.height=20;
//
//                cell.btn_Book.frame=frm;
//            }
//        }
//        [cell.img_close  setImage:[UIImage imageNamed:@"Close"]];
//        [cell.btn_Book setImage:[UIImage imageNamed:@"CloseBook"] forState:UIControlStateNormal];
//        if (allchild.count!=0)
//        {
//            [cell.btn_Book setImage:[UIImage imageNamed:@"OpenBook"] forState:UIControlStateNormal];
//            [cell.img_close  setImage:[UIImage imageNamed:@"Open"]];
//        }
//        else
//        {
//            int flag=[[[_objects objectAtIndex:indexPath.row] valueForKey:KeyChildCount] intValue];
//            if (flag==0)
//            {
//                CGRect frm=cell.btn_Book.frame;
//                frm.size.width=25;
//                frm.size.height=30;
//                cell.btn_Book.frame=frm;
//                [cell.btn_Book setImage:[UIImage imageNamed:@"File"] forState:UIControlStateNormal];
//            }
//            else
//            {
//                [cell.img_close  setImage:[UIImage imageNamed:@"Close"]];
//                CGRect frm=cell.btn_Book.frame;
//                frm.size.width=30;
//                frm.size.height=20;
//                cell.btn_Book.frame=frm;
//            }
//
//            NSString *Query=[NSString stringWithFormat:@"SELECT * FROM WebSite_Category where CategoryID=%@",[[_objects objectAtIndex:indexPath.row] valueForKey:KeyParentID]];
//           NSDictionary *dicchilddata=[self FetchData:Query];
//            
//            NSLog(@"%ld",(long)indexPath.row);
//
//            NSArray *arr_id1=[dicchilddata valueForKey:@"ParentId"];
//            
//            if (arr_id1.count!=0)
//            {
//                NSLog(@"all ids%@",[arr_id1 objectAtIndex:0]);
//                if ([[arr_id1 objectAtIndex:0] isEqualToString:@"0"])
//                {
//                    [cell.img_close  setImage:[UIImage imageNamed:@"Close"]];
//                    CGRect frm=cell.btn_Book.frame;
//                    frm.size.width=30;
//                    frm.size.height=20;
//                    
//                    cell.btn_Book.frame=frm;
//                }
//                else
//                {
//                    CGRect frm=cell.btn_Book.frame;
//                    frm.size.width=25;
//                    frm.size.height=30;
//                    cell.btn_Book.frame=frm;
//                    [cell.btn_Book setImage:[UIImage imageNamed:@"File"] forState:UIControlStateNormal];
//                }
//
//            }
            
//            if (arr_id1.count==0)
//            {
//                [cell.img_close  setImage:[UIImage imageNamed:@"Close"]];
//                CGRect frm=cell.btn_Book.frame;
//                frm.size.width=30;
//                frm.size.height=20;
//                cell.btn_Book.frame=frm;
//            }
//            else
//            {
//                CGRect frm=cell.btn_Book.frame;
//                frm.size.width=25;
//                frm.size.height=30;
//                cell.btn_Book.frame=frm;
//                [cell.btn_Book setImage:[UIImage imageNamed:@"File"] forState:UIControlStateNormal];
//            }
//
////
////            if (arr_id.count!=0)
////            {
////               
////                CGRect frm=cell.btn_Book.frame;
////                frm.size.width=25;
////                frm.size.height=30;
////                cell.btn_Book.frame=frm;
////                [cell.btn_Book setImage:[UIImage imageNamed:@"File"] forState:UIControlStateNormal];
////            }
////            else
////            {
////                [cell.img_close  setImage:[UIImage imageNamed:@"Close"]];
////                CGRect frm=cell.btn_Book.frame;
////                frm.size.width=30;
////                frm.size.height=20;
////                
////                cell.btn_Book.frame=frm;
////            }
//            NSLog(@"%ld",(long)indexPath.row);
//            int flag=[[[_objects objectAtIndex:indexPath.row] valueForKey:KeyChildCount] intValue];
//            if (flag==0)
//            {
//                CGRect frm=cell.btn_Book.frame;
//                frm.size.width=25;
//                frm.size.height=30;
//                cell.btn_Book.frame=frm;
//                [cell.btn_Book setImage:[UIImage imageNamed:@"File"] forState:UIControlStateNormal];
//            }
//            else
//            {
//                NSString *Query=[NSString stringWithFormat:@"SELECT * FROM WebSite_Category where CategoryID=%@",[[_objects objectAtIndex:indexPath.row] valueForKey:KeyParentID]];
//                NSDictionary *dicchilddata=[self FetchData:Query];
//                
//                NSArray *arr_id=[dicchilddata valueForKey:@"Ids"];
//                NSLog(@"all ids%@",arr_id);
//                
//                CGRect frm=cell.btn_Book.frame;
//                frm.size.width=25;
//                frm.size.height=30;
//                cell.btn_Book.frame=frm;
//                [cell.btn_Book setImage:[UIImage imageNamed:@"File"] forState:UIControlStateNormal];
//                    cell.btn_Book.frame=frm;
//                    if (arr_id.count==0)
//                    {
//                        
//                        [cell.img_close  setImage:[UIImage imageNamed:@"Close"]];
//                        CGRect frm=cell.btn_Book.frame;
//                        frm.size.width=30;
//                        frm.size.height=20;
//                    }
//
//                }
    //    }
 //       cell.img_LeftConstraints.constant=indentLevel*20;
        NSLog(@"%f", cell.img_LeftConstraints.constant);
        if (indentLevel==0)
        {
            CGRect frm=cell.img_close.frame;
            NSLog(@"%f",cell.contentView.frame.size.width);
            frm.origin.x=cell.contentView.frame.size.width-30;
            cell.img_close.frame=frm;
            
            CGRect frm_btn=cell.btn_Book.frame;
            frm_btn.origin.x=cell.contentView.frame.size.width-65;
            cell.btn_Book.frame=frm_btn;
            
            CGRect frm_lbl=cell.lbl_name.frame;
            frm_lbl.size.width=cell.contentView.frame.size.width-80;
            cell.lbl_name.frame=frm_lbl;
            [cell.lbl_name setFont:[UIFont fontWithName:@"system font" size:12]];

        }
        else if(indentLevel==1)
        {
            CGRect frm=cell.img_close.frame;
            NSLog(@"%f",cell.contentView.frame.size.width);
            frm.origin.x=cell.contentView.frame.size.width-60;
            cell.img_close.frame=frm;
            
            CGRect frm_btn=cell.btn_Book.frame;
            frm_btn.origin.x=cell.contentView.frame.size.width-90;
            cell.btn_Book.frame=frm_btn;
            
            CGRect frm_lbl=cell.lbl_name.frame;
            frm_lbl.size.width=cell.contentView.frame.size.width-105;
            cell.lbl_name.frame=frm_lbl;
            [cell.lbl_name setFont:[UIFont fontWithName:@"system font" size:12]];

        }
        else if (indentLevel==2)
        {
            CGRect frm=cell.img_close.frame;
            NSLog(@"%f",cell.contentView.frame.size.width);
            frm.origin.x=cell.contentView.frame.size.width-90;
            cell.img_close.frame=frm;
            
            CGRect frm_btn=cell.btn_Book.frame;
            frm_btn.origin.x=cell.contentView.frame.size.width-120;
            cell.btn_Book.frame=frm_btn;
            
            CGRect frm_lbl=cell.lbl_name.frame;
            frm_lbl.size.width=cell.contentView.frame.size.width-135;
            cell.lbl_name.frame=frm_lbl;
            [cell.lbl_name setFont:[UIFont fontWithName:@"system font" size:11]];

            
        }
        else if (indentLevel==3)
        {
            NSLog(@"%f",self.view.frame.size.width);
            
            
            CGRect frm1=cell.img_close.frame;
            frm1.origin.x=self.view.frame.size.width-110;
            cell.img_close.frame=frm1;

                CGRect frm_btn=cell.btn_Book.frame;
                frm_btn.origin.x=self.view.frame.size.width-140;
                cell.btn_Book.frame=frm_btn;
            
           
                CGRect frm_lbl=cell.lbl_name.frame;
                frm_lbl.size.width=cell.contentView.frame.size.width-155;
                cell.lbl_name.frame=frm_lbl;
                [cell.lbl_name setFont:[UIFont fontWithName:@"system font" size:11]];
            
        }
        else  if (indentLevel==4)
        {
            CGRect frm=cell.img_close.frame;
            frm.origin.x=self.view.frame.size.width-130;
            cell.img_close.frame=frm;
            
            CGRect frm_btn=cell.btn_Book.frame;
            frm_btn.origin.x=self.view.frame.size.width-160;
            cell.btn_Book.frame=frm_btn;
            
            CGRect frm_lbl=cell.lbl_name.frame;
            frm_lbl.size.width=cell.contentView.frame.size.width-175;
            cell.lbl_name.frame=frm_lbl;
            [cell.lbl_name setFont:[UIFont fontWithName:@"system font" size:10]];
        }
        else
        {
            CGRect frm=cell.img_close.frame;
            frm.origin.x=self.view.frame.size.width-150;
            cell.img_close.frame=frm;
            
            CGRect frm_btn=cell.btn_Book.frame;
            frm_btn.origin.x=self.view.frame.size.width-180;
            cell.btn_Book.frame=frm_btn;
            
            CGRect frm_lbl=cell.lbl_name.frame;
            frm_lbl.size.width=cell.contentView.frame.size.width-195;
            cell.lbl_name.frame=frm_lbl;
            [cell.lbl_name setFont:[UIFont fontWithName:@"system font" size:9]];
        }
        
        cell.btn_Book.tag=indexPath.row;
        [cell.btn_Book addTarget:self action:@selector(selectparticipate:) forControlEvents:UIControlEventTouchUpInside];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (isSearch==YES)
    {
        NSLog(@"child  count is %@",[[searchArray objectAtIndex:indexPath.row] valueForKey:KeyChildCount]);
        if ([[[searchArray objectAtIndex:indexPath.row] valueForKey:KeyChildCount] intValue]==0)
        {
            DetailViewController *vc=[self.storyboard instantiateViewControllerWithIdentifier:@"DetailViewController"];
            vc.str_id=[[searchArray objectAtIndex:indexPath.row] valueForKey:keyID];
            vc.str_Comment=[[searchArray objectAtIndex:indexPath.row] valueForKey:KeyComment];
            vc.str_name=[[searchArray objectAtIndex:indexPath.row] valueForKey:keyTitle];
            [self.navigationController pushViewController:vc animated:YES];
        }
    }
    else
    {
        NSLog(@"all data  is %@",[_objects objectAtIndex:indexPath.row]);
        NSLog(@"child  count is %@",[[_objects objectAtIndex:indexPath.row] valueForKey:KeyChildCount]);
        if ([[[_objects objectAtIndex:indexPath.row] valueForKey:KeyFileOrFolder] isEqualToString:@"File"])
        {
            DetailViewController *vc=[self.storyboard instantiateViewControllerWithIdentifier:@"DetailViewController"];
            vc.str_id=[[_objects objectAtIndex:indexPath.row] valueForKey:keyID];
            vc.str_Comment=[[_objects objectAtIndex:indexPath.row] valueForKey:KeyComment];
            vc.str_name=[[_objects objectAtIndex:indexPath.row] valueForKey:keyTitle];
            [self.navigationController pushViewController:vc animated:YES];
        }
    }
}
-(void)miniMizeThisRows:(NSArray*)ar forTable:(UITableView *)tableView withIndexpath:(NSIndexPath *)indexPath
{
    NSInteger indentLevel1 = [[[_objects objectAtIndex:indexPath.row] valueForKey:keyIndent] integerValue];
    NSInteger start=indexPath.row+1;

    int start_level = (int) start;
//    int collaps_index = 0;
//    int max = 0;
    NSArray *indentArray = [_objects valueForKey:keyIndent];
    NSMutableArray *remove_index_Arr=[[NSMutableArray alloc] init];
    
    for (int i=start_level; i<indentArray.count; i++)
    {
       // NSArray *indentArray2 = [_objects valueForKey:keyIndent];

        if (indentLevel1<[[indentArray objectAtIndex:i] intValue])
        {
            [remove_index_Arr addObject:[NSString stringWithFormat:@"%d",i]];
            
          //  i=start_level;

        }
        else if (indentLevel1==[[indentArray objectAtIndex:i] intValue])
        {
            break;
        }
    }
        [_objects removeObjectsInRange:NSMakeRange(indexPath.row+1, remove_index_Arr.count)];
        NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
        NSDictionary *oldDict = (NSDictionary *)[_objects objectAtIndex:indexPath.row];
        [newDict addEntriesFromDictionary:oldDict];
        [newDict setValue:[[NSArray alloc] init] forKey:@"keyChildren"];
        [_objects replaceObjectAtIndex:indexPath.row withObject:newDict];
    
        NSArray *indentArray2 = [_objects valueForKey:keyIndent];
        NSLog(@"%@",indentArray2);
}
-(void)selectparticipate:(id)sender
{
    [_txt_search resignFirstResponder];
    UIButton *button=(UIButton *)sender;
    NSIndexPath *indexpath = [NSIndexPath indexPathForRow:button.tag inSection:0];
    NSLog(@"child  count is %@",[[_objects objectAtIndex:indexpath.row] valueForKey:KeyChildCount]);
    
    if ([[[_objects objectAtIndex:indexpath.row] valueForKey:KeyChildCount] intValue]!=0)
    {
 
        UIImage *bookclose = [UIImage imageNamed:@"CloseBook"];
        UIImage *bookopen = [UIImage imageNamed:@"OpenBook"];
    
       
    AllTableViewCell  *cell = [_tbl_alldata dequeueReusableCellWithIdentifier:@"cell"];
    [cell.btn_Book setSelected:YES];

   
    NSDictionary *dic = [_objects objectAtIndex:indexpath.row];
    
    NSInteger indentLevel = [[[_objects objectAtIndex:indexpath.row] valueForKey:keyIndent] integerValue]; //indentLevel of the selected cell
    // NSArray *array=[[_objects objectAtIndex:indexPath.row] valueForKey:keyChildren];
    
    NSArray *indentArray = [_objects valueForKey:keyIndent]; //array of indents which are currently show on table
    
    BOOL indentChek=NO;
    for (int i=0; i<indentArray.count; i++)
    {
        NSString *str_arr=[NSString stringWithFormat:@"%@",[indentArray objectAtIndex:i]];
        NSString *str_lev=[NSString stringWithFormat:@"%ld",(long)indentLevel];
        
        if ([str_arr isEqualToString:str_lev])
        {
            indentChek=YES;
        }
    }
    if ([indentArray containsObject:[NSNumber numberWithInteger:indentLevel]])
    {
        NSLog(@"YES");
    }
    else
    {
        NSLog(@"NO");
        
    }
    BOOL isChildrenAlreadyInserted = [_objects  containsObject:dic[keyChildren]]; //checking contains children
    
    

    for(NSDictionary *dicChildren in dic[keyChildren])
    {
        
        NSInteger index=[_objects indexOfObjectIdenticalTo:dicChildren];
        
        isChildrenAlreadyInserted=(index>0 && index!=NSIntegerMax); //checking contains children
        
        if(isChildrenAlreadyInserted) break;
        
    }
        if ([[sender currentImage] isEqual:bookclose])
        {
            [cell.btn_Book setImage:[UIImage imageNamed:@"OpenBook"] forState:UIControlStateNormal];
            [cell.img_close  setImage:[UIImage imageNamed:@"Open"]];
            // [sender setImage:noSound forState:UIControlStateNormal];
            NSMutableArray *ipsArray = [NSMutableArray new];
            NSString *Query=[NSString stringWithFormat:@"SELECT * FROM WebSite_Category where ParentID=%@",[[_objects objectAtIndex:indexpath.row] valueForKey:keyID]];
            NSDictionary *dicchilddata=[self FetchData:Query];
            
            NSArray *names=[dicchilddata valueForKey:@"Name"];
            NSArray *ids=[dicchilddata valueForKey:@"Ids"];
            NSMutableArray *allchild=[[NSMutableArray alloc] init];
            NSArray *childcount=[dicchilddata valueForKey:@"Childcount"];
            NSArray *Comment=[dicchilddata valueForKey:@"Comment"];
            NSArray *ParentID=[dicchilddata valueForKey:@"ParentId"];
            NSArray *fileOrFolder=[dicchilddata valueForKey:@"FileOrFolder"];


            for (int i=0; i<[names count]; i++)
            {
                NSDictionary *arraydata=
                @{
                  keyIndent:[NSString stringWithFormat:@"%ld",indentLevel+1],
                  keyTitle:[names objectAtIndex:i],
                  keyID:[ids objectAtIndex:i],
                  KeyChildCount:[childcount objectAtIndex:i],
                  KeyComment:[Comment objectAtIndex:i],
                  KeyParentID:[ParentID objectAtIndex:i],
                  KeyFileOrFolder:[fileOrFolder objectAtIndex:i],
                  keyChildren:@[]

                  };
                [allchild addObject:arraydata];
            }
            NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
            NSDictionary *oldDict = (NSDictionary *)[_objects objectAtIndex:indexpath.row];
            [newDict addEntriesFromDictionary:oldDict];
            [newDict setValue:allchild forKey:@"keyChildren"];
            [_objects replaceObjectAtIndex:indexpath.row withObject:newDict];
            
            NSDictionary *dic1 = [_objects objectAtIndex:indexpath.row];
            
            NSInteger count = indexpath.row + 1;
            
            NSLog(@"dic %@",[dic1 valueForKey:keyChildren]);
            
            for (int i = 0; i < [dic1[keyChildren] count]; i++,count++)
            {
                
                NSIndexPath *ip = [NSIndexPath indexPathForRow:count inSection:indexpath.section];
                [ipsArray addObject:ip];
                [_objects insertObject:allchild[i] atIndex:count];
                
            }
            [self.tbl_alldata beginUpdates];
            [self.tbl_alldata insertRowsAtIndexPaths:ipsArray withRowAnimation:UITableViewRowAnimationLeft];
            [self.tbl_alldata endUpdates];

        }
        else if([[sender currentImage] isEqual:bookopen])
        {
            [cell.img_close  setImage:[UIImage imageNamed:@"Close"]];
            [cell.btn_Book setImage:[UIImage imageNamed:@"CloseBook"] forState:UIControlStateNormal];

            [self miniMizeThisRows:_objects[indexpath.row][keyChildren] forTable:self.tbl_alldata withIndexpath:indexpath];

            //  [sender setImage:sound forState:UIControlStateNormal];
        }

    NSLog(@"%@",[_objects  objectAtIndex:indexpath.row]);
    NSLog(@"%@",[_objects valueForKey:keyIndent]);
    [_tbl_alldata reloadData];
        
    }
  //  [[NSUserDefaults standardUserDefaults] setValue:_objects forKey:@"AllIndex"];
   // [[NSUserDefaults standardUserDefaults] synchronize];
}
@end
