//
//  DetailViewController.m
//  Resaleh
//
//  Created by siddharth on 10/18/16.
//  Copyright © 2016 qtm. All rights reserved.
//

#import "DetailViewController.h"
#import "AppDelegate.h"
#import <sqlite3.h>
#import <SVProgressHUD.h>
#import "ViewController.h"
#import "MVYSideMenuController.h"


@interface DetailViewController ()<UIActivityItemSource>
{
    sqlite3 *database;
    NSString *comment;
    NSString *LeftswipeNull;
    NSString *RightswipeNull;
    int numberOfSharedItems;
    BOOL flag;
}
@end

@implementation DetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithPatternImage: [UIImage imageNamed:@"tiledBG"]];
    
    numberOfSharedItems=5;
    [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"sidemenu"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    LeftswipeNull=@"";
    RightswipeNull=@"";
    [self check_FavouriteOrNot:_str_id];
//    _str_favourite=[self FetchData:[NSString stringWithFormat:@"SELECT * FROM WebSite_MetaDataDB where CategoryID=%@",_str_id]];
    
    
    [self set_Data:_str_name :comment];
    _btn_search.layer.cornerRadius=_btn_search.frame.size.height/2;
    _btn_search.layer.masksToBounds=YES;
   _txtdetailview.userInteractionEnabled = YES;
    
    _txtdetailview.editable = NO;
    
    //    if ([_str_flag isEqualToString:@""])
    //    {
    UISwipeGestureRecognizer * swipeleft=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeleft:)];
    swipeleft.direction=UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipeleft];
    
    UISwipeGestureRecognizer * swiperight=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swiperight:)];
    swiperight.direction=UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swiperight];
    //  }


//    self.view.backgroundColor = [UIColor colorWithPatternImage: [UIImage imageNamed:@"tiledBG"]];
//
//    self.txtdetailview.userInteractionEnabled = YES;
//    
//    self.txtdetailview.editable = NO;
//   _btn_search.layer.cornerRadius=_btn_search.frame.size.height/2;
//    _btn_search.layer.masksToBounds=YES;
//    
//    _str_favourite=[self FetchData:[NSString stringWithFormat:@"SELECT * FROM WebSite_Category where CategoryID=%@",_str_id]];
//    NSNumberFormatter *formatter = [NSNumberFormatter new];
//    formatter.locale = [NSLocale localeWithLocaleIdentifier:@"ar"];
//    for (NSInteger i = 0; i < 10; i++)
//    {
//        NSNumber *num = @(i);
//        _str_name = [_str_name stringByReplacingOccurrencesOfString:num.stringValue withString:[formatter stringFromNumber:num]];
//    }
//    formatter.locale = [NSLocale localeWithLocaleIdentifier:@"ar"];
//    for (NSInteger i = 0; i < 10; i++)
//    {
//        NSNumber *num = @(i);
//        comment = [comment stringByReplacingOccurrencesOfString:num.stringValue withString:[formatter stringFromNumber:num]];
//    }
//    NSString *aux = [NSString stringWithFormat:@"<span style=\"font-family: HelveticaNeue-Thin; font-size: 15\">%@</span>",comment];
//
//    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[aux dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:nil error:nil];
//
//    _lbl_navname.text=_str_name;
//    _txtdetailview.attributedText=attrStr;
//    _txtdetailview.textAlignment=NSTextAlignmentRight;
//    
////    CGRect rect      = _txtdetailview.frame;
////    rect.size.height = _txtdetailview.contentSize.height;
////    _txtdetailview.frame   = rect;
//    
//    CGSize size;
//    
//    CGRect frm_lbl=_lbl_details.frame;
//    size=[self getLabelSize:_lbl_details];
//
//    if (size.height>800)
//    {
//        frm_lbl.size.height=size.height-130;
//    }
//    else
//    {
//        frm_lbl.size.height=size.height-10;
//
//    }
//    _lbl_details.frame=frm_lbl;
//    NSLog(@"%@",_str_favourite);
//    
//    if ([_str_favourite isEqualToString:@"0"])
//    {
//        [_btn_favourite setImage:[UIImage imageNamed:@"unfavourite"] forState:UIControlStateNormal];
//        flag=NO;
//
//    }
//    else
//    {
//        [_btn_favourite setImage:[UIImage imageNamed:@"BookMark"] forState:UIControlStateNormal];
//        flag=YES;
//
//    }
//    // Do any additional setup after loading the view.
}
-(void)check_FavouriteOrNot:(NSString *)str_Id
{
    _str_favourite=[self FetchData:[NSString stringWithFormat:@"SELECT * FROM WebSite_Category where CategoryID=%@",_str_id]];
    NSLog(@"%@",_str_favourite);
    
    if ([_str_favourite isEqualToString:@"0"])
    {
        [_btn_favourite setImage:[UIImage imageNamed:@"unfavourite"] forState:UIControlStateNormal];
        flag=NO;
        
    }
    else
    {
        [_btn_favourite setImage:[UIImage imageNamed:@"BookMark"] forState:UIControlStateNormal];
        flag=YES;
        
    }
}
-(void)set_Data:(NSString *)str_Title :(NSString *)str_Comment
{
    NSNumberFormatter *formatter = [NSNumberFormatter new];
    formatter.locale = [NSLocale localeWithLocaleIdentifier:@"ar"];
    for (NSInteger i = 0; i < 10; i++)
    {
        NSNumber *num = @(i);
        str_Title = [str_Title stringByReplacingOccurrencesOfString:num.stringValue withString:[formatter stringFromNumber:num]];
    }
    formatter.locale = [NSLocale localeWithLocaleIdentifier:@"ar"];
    for (NSInteger i = 0; i < 10; i++)
    {
        NSNumber *num = @(i);
        str_Comment = [str_Comment stringByReplacingOccurrencesOfString:num.stringValue withString:[formatter stringFromNumber:num]];
    }
    NSString *aux = [NSString stringWithFormat:@"<span style=\"font-family: HelveticaNeue; font-size: 17\">%@</span>",str_Comment];
    
    NSAttributedString *attrStr = [[NSAttributedString alloc] initWithData:[aux dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:nil error:nil];
    _txtdetailview.attributedText=attrStr;
    _txtdetailview.textAlignment=NSTextAlignmentRight;
    _lbl_navname.text=str_Title;
}
#pragma mark - Swipe Left Method height
-(void)swipeleft:(UISwipeGestureRecognizer*)gestureRecognizer
{
    NSLog(@" Left swipe ");
    
    
    NSMutableDictionary *Dic_Data=[[NSMutableDictionary alloc] init];
    if (![LeftswipeNull isEqualToString:@"Yes"])
    {
        NSInteger metadataID=[_str_id integerValue]-1;
        _str_id=[NSString stringWithFormat:@"%ld",(long)metadataID];
    }
    
    
    Dic_Data=[self FetchSwipeData:[NSString stringWithFormat:@"SELECT  CategoryID, Title, Comment, XmlField,UniqueTitle FROM WebSite_Category where CategoryID=%@",_str_id]];
    if (Dic_Data.count!=0)
    {
        _str_id=[Dic_Data valueForKey:@"Category_ID"];
        comment=[Dic_Data valueForKey:@"Comment"];
        _str_name=[Dic_Data valueForKey:@"Title"];
        LeftswipeNull=@"";
        [self check_FavouriteOrNot:_str_id];
        
        [self set_Data:_str_name :comment];
    }
    else
    {
        LeftswipeNull=@"Yes";
        NSInteger metadataID=[_str_id integerValue]+1;
        _str_id=[NSString stringWithFormat:@"%ld",(long)metadataID];
        
    }
    
    
    //Do what you want here
}
#pragma mark - Swipe Right Method height
-(void)swiperight:(UISwipeGestureRecognizer*)gestureRecognizer
{
    //Do what you want here
    NSLog(@" Right swipe ");
    if (![RightswipeNull isEqualToString:@"Yes"])
    {
        NSInteger metadataID=[_str_id integerValue]+1;
        _str_id=[NSString stringWithFormat:@"%ld",(long)metadataID];
    }
    NSMutableDictionary *Dic_Data=[[NSMutableDictionary alloc] init];
    
    Dic_Data=[self FetchSwipeData:[NSString stringWithFormat:@"SELECT CategoryID, Title, Comment , XmlField , UniqueTitle FROM WebSite_Category where CategoryID=%@",_str_id]];
    
    if (Dic_Data.count!=0)
    {
        _str_id=[Dic_Data valueForKey:@"Category_ID"];
        comment=[Dic_Data valueForKey:@"Comment"];
        _str_name=[Dic_Data valueForKey:@"Title"];
        RightswipeNull=@"";
        [self check_FavouriteOrNot:_str_id];
        
        [self set_Data:_str_name :comment];
        
    }
    else
    {
        RightswipeNull=@"Yes";
        NSInteger metadataID=[_str_id integerValue]-1;
        _str_id=[NSString stringWithFormat:@"%ld",(long)metadataID];
    }
}
-(NSMutableDictionary *)FetchSwipeData:(NSString *)querySQL
{
    //    [SVProgressHUD show];
    //    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
    
    
    AppDelegate *appDelegateLocal = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    const char *dbpath = [[appDelegateLocal getdatabase] UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
        
    {
        
        NSLog(@"%@",querySQL);
        const char *sqlStatement = [querySQL UTF8String];
        
        sqlite3_stmt *statement;
        if( sqlite3_prepare_v2(database, sqlStatement, -1, &statement, NULL) == SQLITE_OK )
        {
            // [SVProgressHUD dismiss];
            
            
            if( sqlite3_step(statement) == SQLITE_ROW )
            {
                NSString *str_Category;
                NSString *str_Title;
                NSString *str_MetadataID;
                
                char *fieldCategory = (char *) sqlite3_column_text(statement,0);
                str_Category = [[NSString alloc] initWithUTF8String: fieldCategory];
                char *fieldTitle = (char *) sqlite3_column_text(statement,1);
                str_Title = [[NSString alloc] initWithUTF8String: fieldTitle];
                char *fieldMetadata = (char *) sqlite3_column_text(statement,2);
                
                if (fieldMetadata==nil)
                {
                    str_MetadataID=@"";
                }
                else
                {
                    str_MetadataID = [[NSString alloc] initWithUTF8String: fieldMetadata];

                }
               
                
                [dic setValue:str_Category forKey:@"Category_ID"];
                [dic setValue:str_Title forKey:@"Title"];
                [dic setValue:str_MetadataID forKey:@"Comment"];
              
            }
        }
        else
        {
            
            //   [SVProgressHUD dismiss];
            
            NSLog( @"Failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(database) );
            
        }
        sqlite3_finalize(statement);
        sqlite3_close(database);
        
    }
    return dic;
}

#pragma mark - Get label height

-(CGSize)getLabelSize :(UILabel *)label
{
    CGSize size = [label.text boundingRectWithSize:CGSizeMake(203, MAXFLOAT)
                   
                                           options:NSStringDrawingUsesLineFragmentOrigin attributes:@{ NSFontAttributeName: label.font }context:nil].size;
    return size;
}
-(NSString *)FetchData:(NSString *)querySQL
{
    [SVProgressHUD show];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];

    AppDelegate *appDelegateLocal = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    const char *dbpath = [[appDelegateLocal getdatabase] UTF8String];
    NSString *str_result=[[NSString alloc] init];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
        
    {

        NSLog(@"%@",querySQL);
        const char *sqlStatement = [querySQL UTF8String];
        
        sqlite3_stmt *statement;
        if( sqlite3_prepare_v2(database, sqlStatement, -1, &statement, NULL) == SQLITE_OK )
        {
            if( sqlite3_step(statement) == SQLITE_ROW )
            {
                char *field1 = (char *) sqlite3_column_text(statement,23);
                str_result = [[NSString alloc] initWithUTF8String: field1];
                
                char *field2 = (char *) sqlite3_column_text(statement,2);
                if (field2==nil)
                {
                    comment=@"";
                }
                else
                {
                    comment = [[NSString alloc] initWithUTF8String: field2];
                }
                
                [SVProgressHUD dismiss];
            }
            else
            {
                [SVProgressHUD dismiss];

                UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"My Alert" message:@"Data is not Found" preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *cancelAction = [UIAlertAction
                                               actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel action")
                                               style:UIAlertActionStyleCancel
                                               handler:^(UIAlertAction *action)
                                               {
                                                   NSLog(@"Cancel action");
                                               }];
                
                UIAlertAction *okAction = [UIAlertAction
                                           actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                           style:UIAlertActionStyleDefault
                                           handler:^(UIAlertAction *action)
                                           {
                                               NSLog(@"OK action");
                                           }];
                
                [alert addAction:cancelAction];
                [alert addAction:okAction];
                [self presentViewController:alert animated:YES completion:nil];
            }
        }
        else
        {
            
            [SVProgressHUD dismiss];

            NSLog( @"Failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(database) );
            
        }
        sqlite3_finalize(statement);
        sqlite3_close(database);
        
    }
    return str_result;
}
-(void )UpdateData:(NSString *)querySQL
{
    [SVProgressHUD show];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];

    
    AppDelegate *appDelegateLocal = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    const char *dbpath = [[appDelegateLocal getdatabase] UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
        
    {
        
        NSLog(@"%@",querySQL);
        const char *sqlStatement = [querySQL UTF8String];
        
        sqlite3_stmt *statement;
        if( sqlite3_prepare_v2(database, sqlStatement, -1, &statement, NULL) == SQLITE_OK )
        {
            
            if (sqlite3_step(statement) != SQLITE_DONE)
            {
                
            }
            else
            {
                [SVProgressHUD dismiss];
                NSLog(@"Updated");
            }
        }
        else
        {
            
            NSLog( @"Failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(database) );
            
        }
        sqlite3_finalize(statement);
        sqlite3_close(database);
        
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btn_favourite:(id)sender
{
    if (flag ==NO)
    {
        [_btn_favourite setImage:[UIImage imageNamed:@"BookMark"] forState:UIControlStateNormal];
        [self UpdateData:[NSString stringWithFormat:@"UPDATE WebSite_Category SET favorite='1' WHERE CategoryID=%@",_str_id]];
        flag=YES;
    }
    else
    {
        [_btn_favourite setImage:[UIImage imageNamed:@"unfavourite"] forState:UIControlStateNormal];
        [self UpdateData:[NSString stringWithFormat:@"UPDATE WebSite_Category SET favorite='0' WHERE CategoryID=%@",_str_id]];
        flag=NO;
    }
}
- (IBAction)btn_back:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];

}
- (IBAction)btn_share:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@""
                                                   message:@"نحوه به اشتراک گذاری آیه مورد نظر را انتخاب کنید"
                                                  delegate:self
                                         cancelButtonTitle:@"اشتراک گذاری با دیگران"
                                         otherButtonTitles:@"افزودن به پژوهشیار", nil];
    [alert show];
}
#pragma mark - AlertView Delegate Method

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex)
    {
        case 0:
        {
            NSString *test = [NSString stringWithFormat:@"%@ \n %@",_lbl_navname.text ,_txtdetailview.text];
            
            NSString *webSite=@"www.makarem.ir";
            
            NSArray *objToShare = @[test,webSite];
            
            UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:objToShare applicationActivities:nil];
            
            activityViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
            
            [self presentViewController:activityViewController animated:YES completion:nil];
        }
            break;
        case 1:
        {
            
        }
            // Do something for button #2
            break;
    }
}
- (id)activityViewControllerPlaceholderItem:(UIActivityViewController *)activityViewController
{
    static UIActivityViewController *shareController;
    static int itemNo;
    if (shareController == activityViewController && itemNo < numberOfSharedItems - 1)
        itemNo++;
    else
    {
        itemNo = 0;
        shareController = activityViewController;
    }
    switch (itemNo)
    {
        case 0: return @""; // intro in email
        case 1: return @""; // email text
        case 2: return [NSURL URLWithString:@"www.makarem.ir"]; // link
        case 3: return nil; // picture
        case 4: return @""; // extra text (via in twitter, signature in email)
        default: return nil;
    }
}
-(id)activityViewController:(UIActivityViewController *)activityViewController itemForActivityType:(NSString *)activityType
{
    // the number of item to share
    static UIActivityViewController *shareController;
    static int itemNo;
    if (shareController == activityViewController && itemNo < numberOfSharedItems - 1)
        itemNo++;
    else
    {
        itemNo = 0;
        shareController = activityViewController;
    }
    NSString *shareText = _txtdetailview.text; // whatever you fancy
    NSURL *shareURL = [NSURL URLWithString:@"www.makarem.ir"];
    
    // twitter
    if ([activityType isEqualToString: UIActivityTypePostToTwitter])
        switch (itemNo) {
            case 0: return nil;
            case 1: return nil; // you can change text for twitter, I add $ to stock symbol inside shareText here, e.g. Hashtags can be added too
            case 2: return shareURL;
            case 3: return nil; // no picture
            case 4: return nil;
            default: return nil;
        }
    
    // email
    else if ([activityType isEqualToString: UIActivityTypeMail])
        switch (itemNo) {
            case 0: return nil;
            case 1: return nil;
            case 2: return shareURL;
            case 3: return nil; // no picture
            case 4: return [@"\r\nCheck it out.\r\n\r\nCheers\r\n" stringByAppendingString: shareText];
            default: return nil;
        }
    else if ([activityType isEqualToString: UIActivityTypePostToFacebook])
    {
        switch (itemNo)
        {
            case 0: return nil;
            case 1: return nil; // you can change text for twitter, I add $ to stock symbol inside shareText here, e.g. Hashtags can be added too
            case 2: return shareURL;
            case 3: return nil; // no picture
            case 4: return nil;
            default: return nil;
        }
    }
    else // Facebook or something else in the future
        switch (itemNo) {
            case 0: return nil;
            case 1: return [NSString stringWithFormat:@"%@ \n %@",_lbl_navname.text,_txtdetailview.text];
            case 2: return shareURL;
            case 3: return nil;
            case 4: return nil;
            default: return nil;
        }
}
@end
