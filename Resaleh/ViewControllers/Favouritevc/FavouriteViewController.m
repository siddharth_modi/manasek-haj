//
//  FavouriteViewController.m
//  Resaleh
//
//  Created by siddharth on 10/19/16.
//  Copyright © 2016 qtm. All rights reserved.
//

#import "FavouriteViewController.h"
#import "favouriteTableViewCell.h"
#import "DetailViewController.h"
#import "AppDelegate.h"
#import <sqlite3.h>
#import <SVProgressHUD.h>
#import "ViewController.h"
#import "MVYSideMenuController.h"

@interface FavouriteViewController ()
{
    sqlite3 *database;
    NSMutableArray *allname;
    NSMutableArray *allID;
    NSMutableArray *allComments;

}
@end

@implementation FavouriteViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    MVYSideMenuController *sideMenuController = [self sideMenuController];
    if (sideMenuController)
    {
        [sideMenuController closeMenu];
    }

    self.navigationController.navigationBarHidden=YES;
    self.view.backgroundColor = [UIColor colorWithPatternImage: [UIImage imageNamed:@"tiledBG"]];

    [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"sidemenu"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    NSMutableDictionary *dic1=[self FetchData:[NSString stringWithFormat:@"SELECT * FROM WebSite_Category where favorite=1"]];
    allname=[[NSMutableArray alloc] init];
    allname=[dic1 valueForKey:@"Name"];
    allID=[[NSMutableArray alloc] init];
    allID=[dic1 valueForKey:@"Ids"];
    allComments=[[NSMutableArray alloc] init];
    allComments=[dic1 valueForKey:@"Comment"];

    NSLog(@"%@",allname);
    NSLog(@"%@",dic1);
    [_tbl_favourite reloadData];
    
    
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    NSMutableDictionary *dic1=[self FetchData:[NSString stringWithFormat:@"SELECT * FROM WebSite_Category where favorite=1"]];
    allname=[[NSMutableArray alloc] init];
    allname=[dic1 valueForKey:@"Name"];
    allID=[[NSMutableArray alloc] init];
    allID=[dic1 valueForKey:@"Ids"];
    allComments=[[NSMutableArray alloc] init];
    allComments=[dic1 valueForKey:@"Comment"];
    
    NSLog(@"%@",allname);
    NSLog(@"%@",dic1);
    [_tbl_favourite reloadData];
}
-(NSMutableDictionary *)FetchData:(NSString *)querySQL
{
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
    [SVProgressHUD show];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];

    AppDelegate *appDelegateLocal = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    const char *dbpath = [[appDelegateLocal getdatabase] UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
        
    {
        NSLog(@"%@",querySQL);
        
        const char *sqlStatement = [querySQL UTF8String];
        
        sqlite3_stmt *statement;
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &statement, NULL) == SQLITE_OK)
        {
            NSMutableArray *allID1=[[NSMutableArray alloc] init];
            NSMutableArray *allname2=[[NSMutableArray alloc] init];
            NSMutableArray *arrComment=[[NSMutableArray alloc] init];
            NSMutableArray *ArrChildCount=[[NSMutableArray alloc] init];
            NSMutableArray *Arrfavourite=[[NSMutableArray alloc] init];
            
            while (sqlite3_step(statement) == SQLITE_ROW)
            {
                
                char *field1 = (char *) sqlite3_column_text(statement,0);
                NSString *field1Str = [[NSString alloc] initWithUTF8String: field1];
                
                
                char *fieldName = (char *) sqlite3_column_text(statement,1);
                NSString *str_name = [[NSString alloc] initWithUTF8String: fieldName];
                
                NSString *newString = [str_name stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                char *field3 = (char *) sqlite3_column_text(statement,2);
                
                NSString *FieldComment;
                if (field3!=nil)
                {
                    FieldComment = [[NSString alloc] initWithUTF8String: field3];
                }
                else
                {
                    FieldComment=@"";
                }
                
                char *field4 = (char *) sqlite3_column_text(statement,16);
                NSString *FieldChildCount = [[NSString alloc] initWithUTF8String: field4];
                
                char *field7 = (char *) sqlite3_column_text(statement,23);
                NSString *Fieldfavourite = [[NSString alloc] initWithUTF8String: field7];
                
                [Arrfavourite addObject:Fieldfavourite];
                [allID1 addObject:field1Str];
                [allname2 addObject:newString];
                [arrComment addObject:FieldComment];
                [ArrChildCount addObject:FieldChildCount];
            }
            
            [dic setValue:allname2 forKey:@"Name"];
            [dic setValue:allID1 forKey:@"Ids"];
            [dic setValue:arrComment forKey:@"Comment"];
            [dic setValue:ArrChildCount forKey:@"Childcount"];
            [dic setValue:Arrfavourite forKey:@"favourite"];
            NSLog(@"dictioary is %@",dic);
            [SVProgressHUD dismiss];


            sqlite3_finalize(statement);
        }
        else
        {
            
            [SVProgressHUD dismiss];
            NSLog( @"Failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(database) );
            
        }
        sqlite3_close(database);
        
    }
    return dic;
}
-(NSString *)FetchName:(NSString *)querySQL
{
    NSString *str_name;
    
    AppDelegate *appDelegateLocal = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    const char *dbpath = [[appDelegateLocal getdatabase] UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSLog(@"%@",querySQL);
        const char *sqlStatement = [querySQL UTF8String];
        sqlite3_stmt *statement;
        if( sqlite3_prepare_v2(database, sqlStatement, -1, &statement, NULL) == SQLITE_OK )
        {
            
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                char *field1 = (char *) sqlite3_column_text(statement,1);
                str_name = [[NSString alloc] initWithUTF8String: field1];
            }
        }
        else
        {
            
            NSLog( @"Failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(database));
        }
        sqlite3_finalize(statement);
        sqlite3_close(database);
        
    }
    return str_name;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    
    return allname.count;
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    favouriteTableViewCell  *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];

       tableView.separatorStyle = UITableViewStylePlain;
    NSLog(@"%ld",(long)indexPath.row);
    NSString *newString = [[allname objectAtIndex:indexPath.row]  stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSNumberFormatter *formatter = [NSNumberFormatter new];
    formatter.locale = [NSLocale localeWithLocaleIdentifier:@"ar"];
    for (NSInteger i = 0; i < 10; i++)
    {
        NSNumber *num = @(i);
        newString = [newString stringByReplacingOccurrencesOfString:num.stringValue withString:[formatter stringFromNumber:num]];
    }
    NSLog(@"%@",newString);
    UIFont *myFont = [ UIFont fontWithName: @"Arial" size: 12.0 ];
    cell.backgroundColor=[UIColor clearColor];
    cell.textLabel.font  = myFont;
    cell.lbl_name.text =newString;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    CGSize size=[self getLabelSize:cell.lbl_name];
    
    CGRect frm=cell.lbl_name.frame;
    frm.size.height=size.height+5;
    
    cell.lbl_name.frame=frm;

    return cell;
}
#pragma mark - Table View Delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
        DetailViewController *vc=[self.storyboard instantiateViewControllerWithIdentifier:@"DetailViewController"];
        vc.str_id=[allID objectAtIndex:indexPath.row];
        vc.str_Comment=[allComments objectAtIndex:indexPath.row];
        vc.str_name=[allname objectAtIndex:indexPath.row];
        vc.str_flag=@"favourite";

        [self.navigationController pushViewController:vc animated:YES];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    favouriteTableViewCell  *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    tableView.separatorStyle = UITableViewStylePlain;
    NSLog(@"%ld",(long)indexPath.row);
    NSString *newString = [[allname objectAtIndex:indexPath.row]  stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSNumberFormatter *formatter = [NSNumberFormatter new];
    formatter.locale = [NSLocale localeWithLocaleIdentifier:@"ar"];
    for (NSInteger i = 0; i < 10; i++)
    {
        NSNumber *num = @(i);
        newString = [newString stringByReplacingOccurrencesOfString:num.stringValue withString:[formatter stringFromNumber:num]];
    }
    
    NSLog(@"%@",newString);
    UIFont *myFont = [ UIFont fontWithName: @"Arial" size: 12.0 ];
    cell.backgroundColor=[UIColor clearColor];
    cell.textLabel.font  = myFont;
    cell.lbl_name.text =newString;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    CGSize size=[self getLabelSize:cell.lbl_name];
    
    
    if (size.height<40)
    {
        return 40;
    }
    else
    {
        return size.height+10;
        
    }
    
    return 0.0f;
    
}
-(CGSize)getLabelSize :(UILabel *)label
{
    CGSize size = [label.text boundingRectWithSize:CGSizeMake(203, MAXFLOAT)
                                           options:NSStringDrawingUsesLineFragmentOrigin attributes:@{ NSFontAttributeName: label.font }context:nil].size;
    return size;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Back button method

- (IBAction)btn_back:(id)sender
{
//    [[NSUserDefaults standardUserDefaults] setValue:@"Yes" forKey:@"sidemenu"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
//    ViewController *vc=[self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    
    [self.navigationController popViewControllerAnimated:YES];
}
@end
