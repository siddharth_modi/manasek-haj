//
//  GuideViewController.h
//  Resaleh
//
//  Created by siddharth on 10/19/16.
//  Copyright © 2016 qtm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GuideViewController : UIViewController<UIPageViewControllerDataSource,UIPageViewControllerDelegate>
- (IBAction)btn_back:(id)sender;

@property (strong, nonatomic) UIPageViewController *pageController;
@property (weak, nonatomic) IBOutlet UIImageView *img_swipe;
@property (weak, nonatomic) IBOutlet UILabel *lbl_description;
- (IBAction)swipeimage:(id)sender;

@end
