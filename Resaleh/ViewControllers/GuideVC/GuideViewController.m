//
//  GuideViewController.m
//  Resaleh
//
//  Created by siddharth on 10/19/16.
//  Copyright © 2016 qtm. All rights reserved.
//

#import "GuideViewController.h"
#import "pageViewController.h"
#import "AppDelegate.h"
#import "ViewController.h"
#import "MVYSideMenuController.h"


@interface GuideViewController ()
{
    NSArray *images;
    BOOL firstTimeLoad;
}
@end
NSInteger imageindex = 0;

@implementation GuideViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.navigationBarHidden=YES;
    self.view.backgroundColor = [UIColor colorWithPatternImage: [UIImage imageNamed:@"tiledBG"]];
    firstTimeLoad=NO;
    images=[[NSArray alloc] initWithObjects:
                     @"help8.png",@"help7.png",@"help6.png",@"help5.png",@"help4.png",@"help3.png",@"help2.png",
                     @"help1.png", nil];
    [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"sidemenu"];
    [[NSUserDefaults standardUserDefaults] synchronize];

    [[NSUserDefaults standardUserDefaults] setValue:images forKey:@"images"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self pagecontroller_Method];
    
    
    MVYSideMenuController *sideMenuController = [self sideMenuController];
    if (sideMenuController)
    {
        [sideMenuController closeMenu];
    }

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)pagecontroller_Method
{
    self.pageController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    
    self.pageController.dataSource = self;
    self.pageController.delegate=self;
    
    [[self.pageController view] setFrame:CGRectMake(0, 60, self.view.frame.size.width, self.view.frame.size.height)];
    
    pageViewController *initialViewController = [self viewControllerAtIndex:0];
    
    NSArray *viewControllers = [NSArray arrayWithObject:initialViewController];
    
    [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
    [self addChildViewController:self.pageController];
    
    //   [[UIApplication sharedApplication].keyWindow addSubview:yourSubview]
    
    [self.view addSubview:self.pageController.view];
    
    NSLog(@"%lu",(unsigned long)index);
    [self.pageController didMoveToParentViewController:self];
}
#pragma mark -
#pragma mark - Page Controll Delegate Method

- (pageViewController *)viewControllerAtIndex:(NSUInteger)index
{
    if (firstTimeLoad==NO)
    {
        index=7;
        firstTimeLoad=YES;
    }
//    else
//    {
//        if (index==0)
//        {
//            index=0;
//        }
//    }
    
//    if (index==0)
//    {
//        index=7;
//    }
//    else if (index==1)
//    {
//      //  index=6;
//    }
//    else if (index==2)
//    {
//        //index=5;
//    }
//    else if (index==3)
//    {
//       // index=4;
//    }
//    else if (index==4)
//    {
//       // index=3;
//    }
//    else if (index==5)
//    {
//       // index=2;
//    }
//    else if (index==6)
//    {
//       // index=1;
//     //   index=0;
//
//    }
//    else
//    {
//        //index=0;
//
//    }

    
    
    pageViewController *childViewController1 = [self.storyboard instantiateViewControllerWithIdentifier:@"pageViewController"];
    childViewController1.index = index;
    
    return childViewController1;
    
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController1 viewControllerBeforeViewController:(UIViewController *)viewController
{
    
    NSUInteger index = [(pageViewController *) viewController index];
    
    NSLog(@"%lu",(unsigned long)index);
    if (index == 0)
    {
        return nil;
    }
    
    // Decrease the index by 1 to return
    index--;
    NSLog(@"%lu",(unsigned long)index);
    
    //  [self.pageControll setCurrentPage:index];
    return [self viewControllerAtIndex:index];
    
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController1 viewControllerAfterViewController:(UIViewController *)viewController
{
    
    NSUInteger index = [(pageViewController *)viewController index];
    
    NSLog(@"%lu",(unsigned long)index);
    
    index++;
    
    NSLog(@"%lu",(unsigned long)index);
    NSLog(@"Array count %lu",(unsigned long)images.count);
    
    if (index == [images count])
    {
        return nil;
    }
    //  [self.pageControll setCurrentPage:index];
    
    return [self viewControllerAtIndex:index];
    
}
- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    
    return [images count];
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{
    // The selected item reflected in the page indicator.
    return 0;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)swipeimage:(id)sender
{
//    NSLog( @"swiped");
//    NSArray *images=[[NSArray alloc] initWithObjects:
//                     @"help1.png",@"help2.png",@"help3.png",@"help4.png",@"help5.png",@"help6.png",@"help7.png"
//                     @"help8.png", nil];
//    
//    UISwipeGestureRecognizerDirection direction =
//    [(UISwipeGestureRecognizer *) sender direction];
//    
//    switch (direction)
//    {
//        case UISwipeGestureRecognizerDirectionLeft:
//            imageindex++;
//            break;
//        case UISwipeGestureRecognizerDirectionRight:
//            imageindex--;
//            break;
//        default:
//            break;
//            
//    }
//    imageindex = (imageindex < 0) ? ([images count] -1):
//    imageindex % [images count];
//    _img_swipe.image = [UIImage imageNamed:[images objectAtIndex:imageindex]];

}
- (IBAction)btn_back:(id)sender
{
//    [[NSUserDefaults standardUserDefaults] setValue:@"Yes" forKey:@"sidemenu"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
//    
//    ViewController *vc=[self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
//    
//    [self.navigationController pushViewController:vc animated:YES];

    [self.navigationController popViewControllerAnimated:YES];
}
@end
