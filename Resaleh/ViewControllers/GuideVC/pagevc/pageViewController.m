//
//  pageViewController.m
//  Resaleh
//
//  Created by siddharth on 10/19/16.
//  Copyright © 2016 qtm. All rights reserved.
//

#import "pageViewController.h"

@interface pageViewController ()

@end

@implementation pageViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    NSMutableArray *array1 = [[[NSUserDefaults standardUserDefaults] objectForKey:@"images"] mutableCopy];
    NSMutableArray *allname=[[NSMutableArray alloc] init];
    NSString *str_help1=@"برای شروع انگشت خود را به سمت راست بکشید";
    NSString *str_help2=@"دسترسی به مسائل و یافتن مطلب مورد نظر از طریق درختواره و جستجوی در آن قرار داده شده است تا سریع ترین حالت ممکن برای کاربر باشد.";
    NSString *str_help3=@"ابزارهای نرم افزار در صفحه درختواره موضوعات قرار داده شده است که از مهمترین آنها می توان به جستجوی پیشرفته و فهرست نشان ها اشاره نمود.";
    NSString *str_help4=@"برای جابجایی بین مسائل (مسئله قبلی و بعدی) می توانید انگشت خود را به راست یا چپ بکشید. همچنین قابلیت بزرگنمایی مطلب در این صفحه وجود دارد.";
    NSString *str_help5=@"فهرست نشان ها از منوی ابزار صفحه درختواره موضوعات قابل دسترس است. برای نمایش مسأله مربوطه از این فهرست کافیست بر روی عنوان مسأله اشاره نمایید.";
    NSString *str_help6=@"برای افزودن به فهرست نشان ها و یا خارج کردن مسأله از فهرست نشان ها می توانید بر روی آیکون مربوطه اشاره نمایید. رنگ قرمز بیان گر نشانه گذاری شده می باشد";
    NSString *str_help7=@"به منظور اشتراک گذاری مسأله می توانید از آیکون مربوطه استفاده نموده و با توجه به درگاه های ارتباطی نرم افزاری موجود بر روی اندرویدتان، می توانید بستر اشتراک گذاری را انتخاب نمایید.";
      NSString *str_help8=@"جستجوی پیشرفته شامل جستجوی کل عبارت: یافتن عین عبارت وارد شده در کادر جستجو، عطفی: یافتن کلمات وارد شده در کادر جستجو در یک مسأله و فصلی: یافتن کلمات به طور مجزا، می باشد.";
    [allname addObject:str_help8];
    [allname addObject:str_help7];
    [allname addObject:str_help6];
    [allname addObject:str_help5];
    [allname addObject:str_help4];
    [allname addObject:str_help3];
    [allname addObject:str_help2];
    [allname addObject:str_help1];

    _lbl_detial.text=[allname objectAtIndex:_index];
    
    _img_swipe.image=[UIImage imageNamed:[array1 objectAtIndex:_index]];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
