//
//  AllTableViewCell.h
//  Resaleh
//
//  Created by siddharth on 10/13/16.
//  Copyright © 2016 qtm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AllTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *img_close;
@property (weak, nonatomic) IBOutlet UIButton *btn_Book;
@property (weak, nonatomic) IBOutlet UILabel *lbl_name;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *img_LeftConstraints;
@end
