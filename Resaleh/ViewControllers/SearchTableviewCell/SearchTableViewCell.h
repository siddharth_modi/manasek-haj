//
//  SearchTableViewCell.h
//  Resaleh
//
//  Created by siddharth on 10/21/16.
//  Copyright © 2016 qtm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lbl_title;
@property (weak, nonatomic) IBOutlet UILabel *lbl_comment;
@property (weak, nonatomic) IBOutlet UILabel *lbl_lastname;
@property (weak, nonatomic) IBOutlet UIImageView *img_pattern;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *comment_height;

@property (weak, nonatomic) IBOutlet UIImageView *img_background;
@end
