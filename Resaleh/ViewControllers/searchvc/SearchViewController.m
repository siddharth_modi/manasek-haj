//
//  SearchViewController.m
//  Resaleh
//
//  Created by siddharth on 10/21/16.
//  Copyright © 2016 qtm. All rights reserved.
//

#import "SearchViewController.h"
#import "AppDelegate.h"
#import <sqlite3.h>
#import "SearchTableViewCell.h"
#import <SVProgressHUD.h>
#import "DetailViewController.h"
#import "ViewController.h"
#import "MVYSideMenuController.h"


@interface SearchViewController ()
{
    BOOL view_open;
    int radio_flag;
    

    sqlite3 *database;
    NSMutableArray *allname;
    NSMutableArray *allParentID;
    NSMutableArray *arr_Comment;
    NSMutableArray *arr_ChildCount;
    NSMutableDictionary *search_data;
    
    NSMutableArray *all_ids;
    NSMutableArray *LastArray;

}
@end
@implementation SearchViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    _activityindicator.hidden=YES;
    _view_filter.hidden=YES;
    _tbl_Allsearch.estimatedRowHeight = 136;
    _tbl_Allsearch.rowHeight = UITableViewAutomaticDimension;
    view_open=NO;
    radio_flag=3;
    
    all_ids=[[NSMutableArray alloc] init];
    self.navigationController.navigationBarHidden=YES;
    
    _btn_search.layer.cornerRadius=5;
  //  _btn_search.layer.borderWidth=1;
   // _btn_search.layer.borderColor=[UIColor colorWithRed:(86.0f/255.0f) green:(173.0f/255.0f) blue:(184.0f/255.0f) alpha:1].CGColor;
    _btn_search.layer.masksToBounds=YES;
    _view_filter.layer.cornerRadius=5;
    _view_filter.layer.masksToBounds=YES;
    
    search_data=[[NSMutableDictionary alloc] init];
    LastArray=[[NSMutableArray alloc] init];

    [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"sidemenu"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    self.view.backgroundColor = [UIColor colorWithPatternImage: [UIImage imageNamed:@"tiledBG"]];

    MVYSideMenuController *sideMenuController = [self sideMenuController];
    if (sideMenuController)
    {
        [sideMenuController closeMenu];
    }

    // Do any additional setup after loading the view.
}

//-(NSMutableDictionary *)FetchData:(NSString *)querySQL
//{
//    _activityindicator.hidden=NO;
//    [_activityindicator startAnimating];
//    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
//    [SVProgressHUD show];
//    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
//    
//    AppDelegate *appDelegateLocal = (AppDelegate *)[[UIApplication sharedApplication]delegate];
//    const char *dbpath = [[appDelegateLocal getdatabase] UTF8String];
//    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
//        
//    {
//        // Fetch data from database..............
//        
//        
//        NSLog(@"%@",querySQL);
//        
//        const char *sqlStatement = [querySQL UTF8String];
//        
//        sqlite3_stmt *statement;
//        if( sqlite3_prepare_v2(database, sqlStatement, -1, &statement, NULL) == SQLITE_OK )
//        {
//            
//            NSMutableArray *allID1=[[NSMutableArray alloc] init];
//            NSMutableArray *allname2=[[NSMutableArray alloc] init];
//            NSMutableArray *arrComment=[[NSMutableArray alloc] init];
//            NSMutableArray *ArrChildCount=[[NSMutableArray alloc] init];
//            NSMutableArray *ArrparentID=[[NSMutableArray alloc] init];
//            
//            while(sqlite3_step(statement) == SQLITE_ROW)
//            {
//                char *field1 = (char *) sqlite3_column_text(statement,0);
//                NSString *field1Str = [[NSString alloc] initWithUTF8String: field1];
//                
//                char *field2 = (char *) sqlite3_column_text(statement,1);
//                NSString *field2Str = [[NSString    alloc] initWithUTF8String: field2];
//                NSString *newString = [field2Str stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//
//                
//                NSNumberFormatter *formatter1 = [NSNumberFormatter new];
//                formatter1.locale = [NSLocale localeWithLocaleIdentifier:@"ar"];
//                for (NSInteger j = 0; j < 10; j++)
//                {
//                    NSNumber *num = @(j);
//                    newString = [newString stringByReplacingOccurrencesOfString:num.stringValue withString:[formatter1 stringFromNumber:num]];
//                }
//                
//                
//                
//                
//                
//                char *field3 = (char *) sqlite3_column_text(statement,2);
//                NSString *FieldComment;
//                if (field3!=nil)
//                {
//                    FieldComment = [[NSString alloc] initWithUTF8String: field3];
//                }
//                else
//                {
//                    FieldComment=@"";
//                }
//                NSNumberFormatter *formatter = [NSNumberFormatter new];
//                formatter.locale = [NSLocale localeWithLocaleIdentifier:@"ar"];
//                for (NSInteger j = 0; j < 10; j++)
//                {
//                    NSNumber *num = @(j);
//                    FieldComment = [FieldComment stringByReplacingOccurrencesOfString:num.stringValue withString:[formatter stringFromNumber:num]];
//                }
//
//                
//                char *field4 = (char *) sqlite3_column_text(statement,16);
//                NSString *FieldChildCount = [[NSString alloc] initWithUTF8String: field4];
//                
//                char *parentID = (char *) sqlite3_column_text(statement,14);
//                NSString *parentID1 = [[NSString alloc] initWithUTF8String: parentID];
//                
//                [allID1 addObject:field1Str];
//                [allname2 addObject:newString];
//                [arrComment addObject:FieldComment];
//                [ArrChildCount addObject:FieldChildCount];
//                [ArrparentID addObject:parentID1];
//            }
//            allParentID=ArrparentID;
//            [dic setValue:allname2 forKey:@"Name"];
//            [dic setValue:allID1 forKey:@"Ids"];
//            [dic setValue:arrComment forKey:@"Comment"];
//            [dic setValue:ArrChildCount forKey:@"Childcount"];
//            [dic setValue:ArrparentID forKey:@"parentIds"];
//            
//            NSLog(@"dictioary is %@",dic);
//            [self getLastString:allParentID];
//            
//            NSLog(@"dictioary is %@",dic);
//
//            
//            
//        }
//        else
//        {
//            
//            NSLog( @"Failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(database) );
//            
//        }
//        sqlite3_finalize(statement);
//        sqlite3_close(database);
//        
//    }
//    return dic;
//}
-(NSMutableDictionary *)FetchData:(NSString *)querySQL
{
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
    [SVProgressHUD show];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    
    AppDelegate *appDelegateLocal = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    const char *dbpath = [[appDelegateLocal getdatabase] UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
        
    {
        // Fetch data from database..............
        
        
        NSLog(@"%@",querySQL);
        
        const char *sqlStatement = [querySQL UTF8String];
        
        sqlite3_stmt *statement;
        if( sqlite3_prepare_v2(database, sqlStatement, -1, &statement, NULL) == SQLITE_OK )
        {
            
            NSMutableArray *allID1=[[NSMutableArray alloc] init];
            NSMutableArray *allname2=[[NSMutableArray alloc] init];
            NSMutableArray *arrComment=[[NSMutableArray alloc] init];
            NSMutableArray *ArrChildCount=[[NSMutableArray alloc] init];
            NSMutableArray *ArrparentID=[[NSMutableArray alloc] init];
            
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                char *field1 = (char *) sqlite3_column_text(statement,0);
                NSString *field1Str = [[NSString alloc] initWithUTF8String: field1];
                
                char *field2 = (char *) sqlite3_column_text(statement,1);
                NSString *field2Str = [[NSString    alloc] initWithUTF8String: field2];
                NSString *newString = [field2Str stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                
                
                NSNumberFormatter *formatter1 = [NSNumberFormatter new];
                formatter1.locale = [NSLocale localeWithLocaleIdentifier:@"ar"];
                for (NSInteger j = 0; j < 10; j++)
                {
                    NSNumber *num = @(j);
                    newString = [newString stringByReplacingOccurrencesOfString:num.stringValue withString:[formatter1 stringFromNumber:num]];
                }
                
                
                
                
                
                char *field3 = (char *) sqlite3_column_text(statement,2);
                NSString *FieldComment;
                if (field3!=nil)
                {
                    FieldComment = [[NSString alloc] initWithUTF8String: field3];
                }
                else
                {
                    FieldComment=@"";
                }
                NSNumberFormatter *formatter = [NSNumberFormatter new];
                formatter.locale = [NSLocale localeWithLocaleIdentifier:@"ar"];
                for (NSInteger j = 0; j < 10; j++)
                {
                    NSNumber *num = @(j);
                    FieldComment = [FieldComment stringByReplacingOccurrencesOfString:num.stringValue withString:[formatter stringFromNumber:num]];
                }
                
                NSString *aux = [NSString stringWithFormat:@"<span style=\"font-family: HelveticaNeue-Thin; font-size: 15\">%@</span>",FieldComment];
                
                NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[aux dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:nil error:nil];
                FieldComment=attrStr.string;
                char *field4 = (char *) sqlite3_column_text(statement,16);
                NSString *FieldChildCount = [[NSString alloc] initWithUTF8String: field4];
                
                char *parentID = (char *) sqlite3_column_text(statement,14);
                NSString *parentID1 = [[NSString alloc] initWithUTF8String: parentID];
                
                [allID1 addObject:field1Str];
                [allname2 addObject:newString];
                [arrComment addObject:FieldComment];
                [ArrChildCount addObject:FieldChildCount];
                [ArrparentID addObject:parentID1];
            }
            allParentID=ArrparentID;
            [dic setValue:allname2 forKey:@"Name"];
            [dic setValue:allID1 forKey:@"Ids"];
            [dic setValue:arrComment forKey:@"Comment"];
            [dic setValue:ArrChildCount forKey:@"Childcount"];
            [dic setValue:ArrparentID forKey:@"parentIds"];
            
            NSLog(@"dictioary is %@",dic);
            if (allParentID.count==0)
            {
                [SVProgressHUD dismiss];
            }
            else
            {
                [self getLastString:allParentID];

            }
            
        }
        else
        {
            [SVProgressHUD dismiss];

            NSLog( @"Failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(database) );
            
        }
        sqlite3_finalize(statement);
        sqlite3_close(database);
        
    }
    return dic;
}

-(NSMutableArray *)FetchParentID_Title:(NSString *)querySQL
{
    
    NSMutableArray *allData=[[NSMutableArray alloc] init];

    AppDelegate *appDelegateLocal = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    const char *dbpath = [[appDelegateLocal getdatabase] UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
        
    {
        // Fetch data from database..............
        NSLog(@"%@",querySQL);
        const char *sqlStatement = [querySQL UTF8String];
        
        sqlite3_stmt *statement;
        if( sqlite3_prepare_v2(database, sqlStatement, -1, &statement, NULL) == SQLITE_OK )
        {
            
            while(sqlite3_step(statement) == SQLITE_ROW)
            {
                char *field1 = (char *) sqlite3_column_text(statement,0);
                NSString *field1Str = [[NSString alloc] initWithUTF8String: field1];
                char *field2 = (char *) sqlite3_column_text(statement,1);
                NSString *field2Str = [[NSString    alloc] initWithUTF8String: field2];
                NSString *newString = [field2Str stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                [allData addObject:field1Str];
                [allData addObject:newString];
                
            }
            [SVProgressHUD dismiss];

            
        }
        else
        {
            
            NSLog( @"Failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(database) );
            
        }
        sqlite3_finalize(statement);
        sqlite3_close(database);
        
    }
    return allData;
}

-(NSString *)FetchLastString:(NSString *)querySQL
{
    NSString *str_lastname=[[NSString alloc] init];
    
    AppDelegate *appDelegateLocal = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    const char *dbpath = [[appDelegateLocal getdatabase] UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
        
    {
        // Fetch data from database..............
//        [SVProgressHUD show];
//        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
        NSLog(@"%@",querySQL);
        
        const char *sqlStatement = [querySQL UTF8String];
        
        sqlite3_stmt *statement;
        if( sqlite3_prepare_v2(database, sqlStatement, -1, &statement, NULL) == SQLITE_OK )
        {
            if( sqlite3_step(statement) == SQLITE_ROW )
            {
                char *field1 = (char *) sqlite3_column_text(statement,0);
                NSString *field2Str = [[NSString    alloc] initWithUTF8String: field1];
                str_lastname = [field2Str stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

                
                NSNumberFormatter *formatter = [NSNumberFormatter new];
                formatter.locale = [NSLocale localeWithLocaleIdentifier:@"ar"];
                for (NSInteger j = 0; j < 10; j++)
                {
                    NSNumber *num = @(j);
                    str_lastname = [str_lastname stringByReplacingOccurrencesOfString:num.stringValue withString:[formatter stringFromNumber:num]];
                }
                
                char *field2 = (char *) sqlite3_column_text(statement,1);

                NSString *field1Str = [[NSString alloc] initWithUTF8String: field2];
                NSLog(@"%@",field1Str);
                
                NSLog(@"dictioary is %@",str_lastname);
                [SVProgressHUD dismiss];
                
            }
        }
        else
        {
        [SVProgressHUD dismiss];

            NSLog( @"Failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(database) );
            
        }
        sqlite3_finalize(statement);
        sqlite3_close(database);
    }
    return str_lastname;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Get Last string of table

-(void)getLastString :(NSMutableArray *)array
{
    for (int i=0; i<array.count; i++)
    {
        NSString *Str_last=[self FetchLastString:[NSString stringWithFormat:@"SELECT Title,ParentID FROM WebSite_Category WHERE CategoryID =%@",[array objectAtIndex:i]]];
        NSLog(@"%@",Str_last);
        [LastArray addObject:Str_last];
    }
    NSLog(@"%@",LastArray);
}
#pragma mark - Tableview Delegate method

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return all_ids.count;
}
-(NSString*)convertHtmlPlainText:(NSString*)HTMLString{
    
    NSData *HTMLData = [HTMLString dataUsingEncoding:NSUTF8StringEncoding];
    NSAttributedString *attrString = [[NSAttributedString alloc] initWithData:HTMLData options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:NULL error:NULL];
    NSString *plainString = attrString.string;
    return plainString;
}
// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    SearchTableViewCell  *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;

//    NSString *str_Comment;
//    NSNumberFormatter *formatter = [NSNumberFormatter new];
//    formatter.locale = [NSLocale localeWithLocaleIdentifier:@"ar"];
//    
//    formatter.locale = [NSLocale localeWithLocaleIdentifier:@"ar"];
//    for (NSInteger j = 0; j < 10; j++)
//    {
//        NSNumber *num = @(j);
//        str_Comment = [[arr_Comment objectAtIndex:indexPath.row] stringByReplacingOccurrencesOfString:num.stringValue withString:[formatter stringFromNumber:num]];
//    }
//    
//    
//    NSString *aux = [NSString stringWithFormat:@"<span style=\"font-family: HelveticaNeue-Thin; font-size: 15\">%@</span>",str_Comment];
//    
//    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[aux dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:nil error:nil];
//    NSString * htmlString = [NSString stringWithFormat:@"<span style=\"font-family: HelveticaNeue-Thin; font-size: 15\">%@</span>",str_Comment];
//    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    
//    NSString *aux = [NSString stringWithFormat:@"<span style=\"font-family: HelveticaNeue-Thin; font-size: 15\">%@</span>",str_Comment];
//    
//    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[aux dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:nil error:nil];
    
    cell.lbl_comment.text=[arr_Comment objectAtIndex:indexPath.row];
    cell.lbl_comment.textAlignment= NSTextAlignmentRight;
    cell.lbl_comment.numberOfLines = 0;
    NSString *str_Name;
    
        str_Name=[allname objectAtIndex:indexPath.row];
        NSNumberFormatter *formatter = [NSNumberFormatter new];

    
        for (NSInteger i = 0; i < 10; i++)
        {
            NSNumber *num = @(i);
            str_Name = [[allname objectAtIndex:indexPath.row] stringByReplacingOccurrencesOfString:num.stringValue withString:[formatter stringFromNumber:num]];
        }
    
    cell.lbl_title.text=str_Name;
    
    NSLog(@"label title height %lu",(unsigned long)[str_Name length]);

    
    NSString *str_lastname;
    NSNumberFormatter *formatter1 = [NSNumberFormatter new];

    formatter1.locale = [NSLocale localeWithLocaleIdentifier:@"ar"];
    
    formatter1.locale = [NSLocale localeWithLocaleIdentifier:@"ar"];
    for (NSInteger j = 0; j < 10; j++)
    {
        NSNumber *num = @(j);
        str_lastname = [[LastArray objectAtIndex:indexPath.row] stringByReplacingOccurrencesOfString:num.stringValue withString:[formatter1 stringFromNumber:num]];
    }

    cell.lbl_lastname.text=str_lastname;
    
    if (indexPath.row%2==0)
    {
       
        cell.img_background.backgroundColor=[UIColor whiteColor];

    }
    else
    {
         cell.img_background.backgroundColor=[UIColor colorWithRed:(255.0f/255.0f) green:(255.0f/255.0f) blue:(217.0f/255.0f) alpha:1];
    }
    
    cell.img_background.layer.cornerRadius=10;
    cell.img_background.layer.masksToBounds=YES;
    cell.img_background.layer.masksToBounds = NO;
    cell.img_background.layer.shadowRadius=2;
    cell.img_background.layer.shadowColor = [UIColor darkGrayColor].CGColor;
    cell.img_background.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
    cell.img_background.layer.shadowOpacity = 0.5f;


    UIImage * backgroundImg = [UIImage imageNamed:@"devide"];
    
    backgroundImg = [backgroundImg resizableImageWithCapInsets:UIEdgeInsetsMake(20, 60, 30, 1)];
    
    [cell.img_pattern setImage:backgroundImg];
    
    
    cell.comment_height.constant=0;
    
    
   // cell.comment_height.constant = size1.height;
    
  //  cell.lbl_title_height.constant=size1.height;

    
//    if ( size.height>400 && size.height<820)
//    {
//        cell.comment_height.constant=size.height-100;
//        cell.view_height.constant=cell.comment_height.constant+50;
//    }
//    else if ( size.height<40)
//    {
//        cell.comment_height.constant=size.height+50;
//        cell.view_height.constant=cell.comment_height.constant+50;
//
//        
//    }
//    else
//    {
//        cell.comment_height.constant=size.height+50;
//        cell.view_height.constant=cell.comment_height.constant+50;
//
//    }
//    cell.view_height.constant=cell.comment_height.constant+50;
    
//    if (size.height>350)
//    {
//        cell.comment_height.constant=size.height-10;
//
//    }
//    else
//    {
//        cell.comment_height.constant=size.height;
//
//    }
//    NSLog(@" Label height %f", cell.comment_height.constant);

   

    
//    if (size.height>400)
//    {
//        cell.view_height.constant=size.height-100;
//    }
//    else if (size.height>350)
//    {
//         cell.view_height.constant=size.height+60;
//        
//    }
//    else
//    {
//        cell.view_height.constant=size.height+80;
//
//    }

    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    DetailViewController *vc=[self.storyboard instantiateViewControllerWithIdentifier:@"DetailViewController"];
    vc.str_id=[all_ids objectAtIndex:indexPath.row];
    vc.str_name=[allname objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:vc animated:YES];
}
#pragma mark - Get label height

-(CGSize)getLabelSize :(UILabel *)label
{
    CGSize size = [label.text boundingRectWithSize:CGSizeMake(203, MAXFLOAT)
                   
                                                      options:NSStringDrawingUsesLineFragmentOrigin attributes:@{ NSFontAttributeName: label.font }context:nil].size;
    return size;
}
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return  UITableViewAutomaticDimension
//}
//    SearchTableViewCell  *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
//    cell.selectionStyle=UITableViewCellSelectionStyleNone;
//    
//   
//    cell.lbl_title.text=[allname objectAtIndex:indexPath.row];
//    NSString *aux = [NSString stringWithFormat:@"<span style=\"font-family: HelveticaNeue-Thin; font-size: 15\">%@</span>",[arr_Comment objectAtIndex:indexPath.row]];
//    
//    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[aux dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:nil error:nil];
//    
//  
//    cell.lbl_comment.attributedText=attrStr;
//
//    cell.comment_height.constant=0;
//
////    CGRect rect  = cell.txtview_Comment.frame;
////    rect.size.height = cell.txtview_Comment.contentSize.height;
//    
//    CGSize size = [self getLabelSize:cell.lbl_comment];
//
//   // cell.comment_height.constant = size.height+10;
//    
////    SearchTableViewCell  *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
////    cell.selectionStyle=UITableViewCellSelectionStyleNone;
////    cell.lbl_title.text=[allname objectAtIndex:indexPath.row];
////    cell.lbl_comment.text=[arr_Comment objectAtIndex:indexPath.row];
////    cell.lbl_lastname.text=[LastArray objectAtIndex:indexPath.row];
////    
////    
////    
////    CGSize size = [cell.lbl_comment.text boundingRectWithSize:CGSizeMake(203, MAXFLOAT)
////                   
////                                                     options:NSStringDrawingUsesLineFragmentOrigin attributes:@{ NSFontAttributeName: cell.lbl_comment.font }context:nil].size;
////    
////    CGRect frm_view=cell.view_cell.frame;
////    frm_view.size.height=cell.lbl_comment.frame.origin.y+size.height+cell.lbl_sleepline.frame.origin.y+cell.lbl_sleepline.frame.size.height+cell.lbl_lastname.frame.origin.y+cell.lbl_lastname.frame.size.height+10;
////    cell.view_cell.frame=frm_view;
////    
////    NSLog(@" Label height %f",size.height);
////    
////    if ( size.height>400 && size.height<820)
////    {
////        cell.comment_height.constant=size.height-100;
////
////        cell.view_height.constant=cell.comment_height.constant+50;
////
////    }
////    else if ( size.height<40)
////    {
////        cell.comment_height.constant=size.height+50;
////        cell.view_height.constant=cell.comment_height.constant+50;
////
////    }
////    else
////    {
////        cell.comment_height.constant=size.height+50;
////        cell.view_height.constant=cell.comment_height.constant+50;
////    }
////    
////
//////    if (size.height>350)
//////    {
//////        cell.comment_height.constant=size.height-10;
//////        
//////    }
//////    else
//////    {
//////        cell.comment_height.constant=size.height;
//////        
//////    }
//////    cell.comment_height.constant=size.height;
//////
//////    NSLog(@" Label height %f", cell.comment_height.constant);
//////
//////    cell.view_height.constant=size.height+100;
//////    
//////    
//////    if (size.height>400)
//////    {
//////        return  cell.view_height.constant=size.height-90;
//////    }
//////    else if (size.height>350)
//////    {
//////        return   cell.view_height.constant=size.height+50;
//////        
//////    }
//////    else
//////    {
//////      return cell.view_height.constant-10;
//////    }
////
//    return  0;
//}
#pragma mark - Button Back
- (IBAction)btn_back:(id)sender
{
//    [[NSUserDefaults standardUserDefaults] setValue:@"Yes" forKey:@"sidemenu"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
//    ViewController *vc=[self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
//    
//    [self.navigationController pushViewController:vc animated:YES];
    [self.navigationController popViewControllerAnimated:YES];

}

- (IBAction)btn_search:(id)sender
{
    NSLog(@" Total Text length is %lu",[[_txt_search text] length]);
    if([[_txt_search text] length]<2)
    {
        UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"" message:@"لطفا بیش تر از یک کاراکتر جستجو کنید !" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       NSLog(@"OK action");
                                   }];
        
        [alert addAction:okAction];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else
    {
        [_txt_search resignFirstResponder];
        NSString * str = _txt_search.text;
        NSArray * arr = [str componentsSeparatedByString:@" "];
        NSLog(@"Array values are : %@",arr);
        
        if (radio_flag==1)
        {
            NSLog(@"hmm");
            NSString *query;
            for (int i=0; i<arr.count; i++)
            {
                NSString *add_laststring;
                NSLog(@"%@",query);
                if (i>=1)
                {
                     add_laststring=[NSString stringWithFormat:@" and Comment like '%%%@%%'",[arr objectAtIndex:i]];
                }
                if (i==0)
                {
                    query=[NSString stringWithFormat:@"SELECT * FROM WebSite_Category WHERE  Comment like '%%%@%%'",[arr objectAtIndex:i]];
                }
                else
                {
                    query=[query stringByAppendingString:add_laststring];

                }
                NSLog(@"%@",query);
            }
            NSLog(@"%@",query);

            search_data=[self FetchData:query];
            all_ids=[search_data valueForKey:@"Ids"];
            allname=[search_data valueForKey:@"Name"];
            arr_Comment=[search_data valueForKey:@"Comment"];
            [_tbl_Allsearch reloadData];
            [_activityindicator stopAnimating];
            _activityindicator.hidden=YES;

        }
        else if (radio_flag==2)
        {
            NSString *query;
            for (int i=0; i<arr.count; i++)
            {
                NSString *add_laststring;
                NSLog(@"%@",query);
                if (i>=1)
                {
                    add_laststring=[NSString stringWithFormat:@" or Comment like '%%%@%%'",[arr objectAtIndex:i]];
                }
                if (i==0)
                {
                    query=[NSString stringWithFormat:@"SELECT * FROM WebSite_Category WHERE  Comment like '%%%@%%'",[arr objectAtIndex:i]];
                }
                else
                {
                    query=[query stringByAppendingString:add_laststring];
                    
                }
                NSLog(@"%@",query);
            }
            NSLog(@"%@",query);
            
            search_data=[self FetchData:query];
            all_ids=[search_data valueForKey:@"Ids"];
            allname=[search_data valueForKey:@"Name"];
            arr_Comment=[search_data valueForKey:@"Comment"];
            [_tbl_Allsearch reloadData];
            [_activityindicator stopAnimating];
            _activityindicator.hidden=YES;

        }
        else
        {
            search_data=[self FetchData:[NSString stringWithFormat:@"SELECT * FROM WebSite_Category WHERE Comment like '%%%@%%'",_txt_search.text]];
            all_ids=[search_data valueForKey:@"Ids"];
            allname=[search_data valueForKey:@"Name"];
            arr_Comment=[search_data valueForKey:@"Comment"];
            [_tbl_Allsearch reloadData];
            
            [_activityindicator stopAnimating];
            _activityindicator.hidden=YES;
        }
    }
}
- (IBAction)btn_down:(id)sender
{
    if (view_open==NO)
    {
        _view_filter.hidden=NO;
        [_btn_down setImage:[UIImage imageNamed:@"up_arrow"] forState:UIControlStateNormal];
        view_open=YES;
    }
    else
    {
        _view_filter.hidden=YES;
        [_btn_down setImage:[UIImage imageNamed:@"down_arrow"] forState:UIControlStateNormal];

        view_open=NO;
    }

}

- (IBAction)btn_or:(id)sender
{
    _img_or.image=[UIImage imageNamed:@"fill_radio"];
    _img_and.image=[UIImage imageNamed:@"unfill_radio"];
    _img_normal.image=[UIImage imageNamed:@"unfill_radio"];
    radio_flag=1;
}

- (IBAction)btn_and:(id)sender
{
    _img_or.image=[UIImage imageNamed:@"unfill_radio"];
    _img_and.image=[UIImage imageNamed:@"fill_radio"];
    _img_normal.image=[UIImage imageNamed:@"unfill_radio"];
    radio_flag=2;
}
- (IBAction)btn_normal:(id)sender
{
    _img_or.image=[UIImage imageNamed:@"unfill_radio"];
    _img_and.image=[UIImage imageNamed:@"unfill_radio"];
    _img_normal.image=[UIImage imageNamed:@"fill_radio"];
   
    radio_flag=3;
}
#pragma mark - Textfield Delegate method

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}// called when 'return' key pressed. return NO to ignore.

@end
