//
//  main.m
//  Resaleh
//
//  Created by siddharth on 10/12/16.
//  Copyright © 2016 qtm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool
    {
        NSArray* languages = [NSArray arrayWithObjects:@"ar", nil];
        [[NSUserDefaults standardUserDefaults] setObject:languages forKey:@"AppleLanguages"];
        
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
